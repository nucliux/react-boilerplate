#!/bin/bash
function _menu() {
    echo "Options:"
    echo
    echo "1) Make <Component/>"
    echo "2) Remove <Component/>"
    echo "9) Exit"
    echo ""
}
reset
opc=0
# Loop allow values until 9.
until [ $opc -eq 9 ]
do
    case $opc in
        # Make <Component/>
        1)
        	echo "Insert your component name and press [ENTER]:"
            read Component
            if  [ ${#Component} == 0 ]; then
                echo "Please insert some characters, quitting."
                exit 1
            else
                ## Instances variables ##
                Component="$(tr '[:lower:]' '[:upper:]' <<< ${Component:0:1})${Component:1}"
                Component_lowercase=$(echo "$Component" | tr '[:upper:]' '[:lower:]')
                Component_uppercase=$(echo "$Component" | tr '[:lower:]' '[:upper:]')
                echo "Creating <$Component/>"
                ## Copy from _scallfolds folder component. ##
                mkdir -p src/components/$Component
                cp -r src/_scallfolds/Component/components/New/* src/components/$Component
                ## Rename files ##
                for file in src/components/$Component/*; do
                    if [[ $file == *"New"* ]]; then
                        rename="${file/New/$Component}"
                        mv $file $rename 2>/dev/null
                    fi
                    if [[ $file == *"new"* ]]; then
                        rename="${file/new/$Component_lowercase}"
                        mv $file $rename 2>/dev/null
                    fi
                done
                ## Replace strings inside. ##
                if [ "$(uname)" == "Darwin" ]; then
                    find src/components/$Component -type f -exec sed -i -e 's/__new__/'$Component'/g' {} \;
                    find src/components/$Component -type f -exec sed -i -e 's/_new_/'${Component_lowercase}'/g' {} \;
                    find src/components/$Component -type f -exec sed -i -e 's/__NEW__/'${Component_uppercase}'/g' {} \;
                elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
                    find src/components/$Component -type f -exec sed -i -e 's/__new__/'"$Component"'/g' {} \;
                    find src/components/$Component -type f -exec sed -i -e 's/_new_/'"${Component_lowercase}"'/g' {} \;
                    find src/components/$Component -type f -exec sed -i -e 's/__NEW__/'"${Component_uppercase}"'/g' {} \;
                fi

                ## Update src/componets/index.js ##
                file='src/components/index.js'
                # Insert first line.
                sed -i "1 i\import $Component from './$Component/$Component';" $file
                count=$(wc -l <$file)
                sed -i "$count i\    $Component,\\n};" $file
                # Remove last line.
                sed -i '$ d' $file

                ## Update src/reducers ##
                file='src/reducers.js'
                sed -i "3 i\import $Component from './components/${Component}/${Component_lowercase}Reducer';" $file
                count=$(wc -l <$file)
                sed -i "$count i\    $Component,\\n});" $file
                sed -i '$ d' $file
                ## Review file on terminal when is progreming ##
                # while IFS='' read -r line || [[ -n "$line" ]]; do
                #     echo "Text read from file: $line"
                # done < "src/reducers.js"
                echo "Finish"
            fi
            read -n 1 -s -p "Press any key to continue"
            reset
            _menu
            ;;
        2)
			## Listing folder with name folders components. ##
			echo "List of components:"
			cd ./src/components
			arr=(./*)
			for ((i=0; i<${#arr[@]}; i++)); do
			    # Directory?
			    initial="$(echo ${arr[$i]} | head -c 3)"
			    if [ -d "${arr[$i]}" ] && [ $initial != "./_" ]; then
			    	echo "  <${arr[$i]: 2} />"
				fi
			done
			cd ../../
			## Wait user enter text.
			echo ""
			echo "Insert component name (without < />) and press [ENTER]:"
            read Component
            if  [ ${#Component} == 0 ]; then
                echo "Please insert some characters, quitting."
                exit 1
            else
            	Component="$(tr '[:lower:]' '[:upper:]' <<< ${Component:0:1})${Component:1}"
            	echo "Removing files and lines files for component: <${Component} />"
            	## Update src/reducers ##
                file='./src/reducers.js'
                tmpfile='./tmpfile'
            	awk "!/${Component}/{print}" $file > $tmpfile && mv $tmpfile $file
            	## Update src/components/index.js ##
            	file='./src/components/index.js'
                tmpfileTwo='./tmpfileTwo'
            	awk "!/${Component}/{print}" $file > $tmpfileTwo && mv $tmpfileTwo $file
            	## Remove folder entire component ##
            	rm -rf $folder "./src/components/${Component}"
            	echo "Finish"
            fi
            read -n 1 -s -p "Press any key to continue"
			reset
			_menu
			;;
        *)
            # Another different choice from up.
            _menu
            ;;
    esac
    read opc
done
