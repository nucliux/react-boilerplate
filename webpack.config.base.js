import Path from './path.js';
import webpack from 'webpack';
import { dependencies as externals } from './package.json';

module.exports = {
    // externals: Object.keys(externals || {}),
    module: {
        noParse: [/test/, /dist/],
        rules: [{
            test: /\.jsx?$/,
            exclude: /node_modules|\.git/,
            use: [{
                loader: 'babel-loader',
                query: {
                    presets: [
                        'react',
                        'es2015',
                        'stage-0',
                        'stage-2',
                    ],
                    plugins: [
                        'react-html-attrs',
                        'transform-class-properties',
                        'transform-decorators-legacy',
                    ],
                },
            }],
        },
        {
            test: /\.(jpe?g|png|gif|svg)$/i,
            loader: 'file-loader',
            options: {
                name: '[path][name].[ext]',
                publicPath: ''
            }
        },
        ],
    },

    output: {
        path: Path.src,
        filename: 'bundle.js',
    },

    /**
     * Determine the array of extensions that should be used to resolve modules.
     */
    resolve: {
        extensions: ['.js', '.jsx', '.json'],
        modules: [
            Path.src,
            Path.node_modules,
        ],
        alias: {
            Path: `${__dirname  }/path.js`,
            api: Path.api,
            app: Path.app,
            conf: Path.conf,
            helpers: Path.helpers,
            layouts: Path.layouts,
            components: Path.components,
            'stylesBase.css': Path.styles_base,
            'stylesResponsive.css': Path.styles_responsive,
            store: Path.store,
            reducers: Path.reducers,
            routes: Path.routes,
        }
    },

    plugins: [
        new webpack.EnvironmentPlugin({
    	    NODE_ENV: 'production'
    	}),

        new webpack.NamedModulesPlugin(),
    ],

};
