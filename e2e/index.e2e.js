import { ClientFunction, Selector } from 'testcafe';
import uaParser from 'ua-parser-js';
import Path from './../path';
import OS from 'os';

const platform = OS.platform();
const getUA = ClientFunction(() => navigator.userAgent);
const pages = [
    'http://localhost:8080',
    `file:///${Path.dist}/index.html`,
];

for (let i = 0; i < pages.length; i++) {
    const url = pages[i];

    fixture`My fixture | ${url}`
        .page`${url}`
        .beforeEach(async t => {
            const ua = await getUA();
            const folder = url.split('/')[2].split('.')[0];
            t.ctx.folder = (folder === '') ? `${platform}/index.html` : `${platform}/${folder}`;
            t.ctx.browser = uaParser(ua).browser.name.toLowerCase();
            t.ctx.idHome = Selector('#home');
            t.ctx.idSample2 = Selector('#sample2');
        });

    test('Click links and take some screenshots', async t => {
        await t
            .click(t.ctx.idHome)
            .click(t.ctx.idSample2)
            .takeScreenshot(`${t.ctx.folder}/${t.ctx.browser}/1_Sample2.png`)
            .click(t.ctx.idHome)
            .takeScreenshot(`${t.ctx.folder}/${t.ctx.browser}/2_Home.png`);
    });
}
