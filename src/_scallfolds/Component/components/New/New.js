import 'store';
import React from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import responsive from 'stylesResponsive.css';
import st from './_new_.css';

@connect((store) => ({}))
export default class __new__ extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        return (<div id="__new__">__new__ Component created!</div>);
    }
}
