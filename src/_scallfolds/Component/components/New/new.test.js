import store from 'store';
import React from 'react';
import { Provider, connect } from 'react-redux';
import { shallow, mount, render } from 'enzyme';
import renderer from 'react-test-renderer';
import __new__ from './__new__';
import * as actions from './_new_Actions';
import reducer from './_new_Reducer';
import { _new_, API } from 'api';
import Conf from 'conf';
import { BrowserRouter } from 'react-router-dom';

describe('__new__', () => {
    // Variables available for all tests.
    const reducerState = store.getState().__new__;

    describe('visual', () => {
        // Variables
        let enzymeComponent = null;
        let tree = null;
        beforeAll(() => {
            const element = <Provider store={ store }><BrowserRouter><__new__ /></BrowserRouter></Provider>;
            enzymeComponent = mount(element);
            tree = renderer.create(element).toJSON();
            expect(tree).toMatchSnapshot();
        });
        test('Sring - Acceso portal', () => {
            const str = '__new__ Component created!';
            expect(enzymeComponent.find('#__new__').text()).toContain(str);
        });
    });

    describe('reducers', () => {
        test('LOADING_ACTIVE', () => {
            const response = reducer(reducerState, { type: 'LOADING_ACTIVE' });
            expect(response.active).toEqual(true);
        });
        test('LOADING_INACTIVE', () => {
            const response = reducer(reducerState, { type: 'LOADING_INACTIVE' });
            expect(response.active).toEqual(false);
        });
    });

    describe('actions', () => {
        test('success:get__new__()', (done) => {
            const unsubscribe = store.subscribe(() => {
                const props = store.getState().__new__;
                try {
                    expect(props.active).toEqual(true);
                } catch (e) {
                    console.log(e);
                    throw Error;
                }
                done();
            });
            store.dispatch(actions.get__new__());
        });
    });
});
