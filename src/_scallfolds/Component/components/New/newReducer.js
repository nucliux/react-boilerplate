export default function reducer(state = {
    error: false,
}, action) {
    switch (action.type) {
        case '__NEW___ADD_ERROR': {
            return { ...state, error: action.payload };
        }

        case '__NEW___REMOVE_ERROR': {
            return { ...state, error: false };
        }
    }
    return state;
}
