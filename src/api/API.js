import Axios from 'axios';
import Conf from 'conf';
import store from 'store';
import * as actions from './apiActions';

export default class API {
    constructor(apiUrl) {
        this.baseURL = apiUrl;

        this.Connection = Axios.create({
            baseURL: this.baseURL,
            timeout: 5000,
        });
        // Add a request interceptor
        this.Connection.interceptors.request.use((config) => {
            const reducerState = store.getState().API;
            // BlockToSync is false.
            if (!Conf.blockToSync) {
                return config;
            }
            // BlockToSync is true.
            if (!reducerState.requesting && Conf.blockToSync) {
                store.dispatch(actions.requestActive());
                return config;
            }
        }, (error) => {
            // Do something with request error
            store.dispatch(actions.requestInactive());
            return Promise.reject(error);
        });
        // Add a response interceptor
        this.Connection.interceptors.response.use((response) => {
            store.dispatch(actions.requestInactive());
            store.dispatch(actions.activeNetwork());
            store.dispatch(actions.removeErrorTimeout());
            return response;
        }, (error) => {
            // Do something with response error
            store.dispatch(actions.requestInactive());
            store.dispatch(actions.inactiveNetwork());
            return Promise.reject(error);
        });
    }

    getConnection() {
        return this.Connection;
    }

    get(uri, callback) {
        this.Connection.get(uri)
            .then(callback)
            .catch(this.handleError);
    }

    post(uri, requestBody, callback) {
        this.Connection.post(uri, requestBody)
            .then(callback)
            .catch(this.handleError);
    }

    put(uri, requestBody, callback) {
        this.Connection.put(uri, requestBody)
            .then(callback)
            .catch(this.handleError);
    }

    getWithoutErrors(uri, callback) {
        this.Connection.get(uri, {
            validateStatus: (status) => {
                callback(status);
            }
        })
            .catch((error) => {
                if (error.response) {
                    callback(error.response.status);
                } else {
                    callback(404);
                }
            });
    }

    handleError(error) {
        if (error.response) {
            // The request was made and the server responded with a status code
            // that falls out of the range of 2xx
            if (Conf.debug) {
                console.log(error.response.data);
                console.log(error.response.status);
                console.log(error.response.headers);
            }
        } else if (!error.status) {
            //  store.dispatch(actions.inactiveNetwork());
        } else if (error.request) {
            // The request was made but no response was received
            // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
            // http.ClientRequest in node.js
            store.dispatch(actions.errorTimeout(error.request));
        } else {
            // Something happened in setting up the request that triggered an Error
            console.log('Error', error.message);
        }
        if (Conf.debug) {
            console.log(error.config);
        }
    }
}
