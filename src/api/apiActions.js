import store from 'store';

export function errorTimeout(obj) {
    return { type: 'API_ADD_ERROR_TIMEOUT', payload: obj };
}

export function removeErrorTimeout() {
    return { type: 'API_REMOVE_ERROR_TIMEOUT' };
}

export function activeNetwork() {
    return { type: 'API_NETWORK_ACTIVE' };
}

export function inactiveNetwork() {
    const reducerState = store.getState();
    return (dispatch) => {
        const error = 'Error: No se puede conectar a la red.';
        const payload = { mensaje: error };
        // Login.
        if (!reducerState.Login.isLogged) {
            dispatch({ type: 'LOGIN_STOP_LOADING' });
            dispatch({ type: 'LOGIN_ADD_ERROR', payload });
        // Home
        } else {
            const section = reducerState.Menu.action;
            switch (section) {
                case 'balances': {
                    dispatch({ type: 'BALANCES_ADD_ERROR', payload });
                    break;
                }
                case 'movements': {
                    dispatch({ type: 'MOVEMENTS_ADD_ERROR', payload });
                    break;
                }
                case 'statements': {
                    dispatch({ type: 'STATEMENTS_ADD_ERROR', payload });
                    break;
                }
                case 'password': {
                    dispatch({ type: 'LOADING_INACTIVE' });
                    dispatch({ type: 'PASSWORD_ADD_ERROR', payload });
                    break;
                }
            }
        }

        return dispatch({ type: 'API_NETWORK_INACTIVE' });
    };
}

export function requestActive() {
    return { type: 'API_REQUESTING_ACTIVE' };
}

export function requestInactive() {
    return { type: 'API_REQUESTING_INACTIVE' };
}
