import { Home } from 'layouts';
import { RouteNoMatch } from 'helpers';

const routes = [
    {
        path: '/',
        component: Home,
        exact: true,
    },
    // This parameter must be on last value.
    {
        component: RouteNoMatch,
    },
];

export default routes;
