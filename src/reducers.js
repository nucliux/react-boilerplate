import { combineReducers } from 'redux';

import API from './api/apiReducer';
import Home from './components/_layouts/Home/homeReducer';
import Loading from './components/_helpers/Loading/loadingReducer';

export default combineReducers({
    API,
    Home,
    Loading,
});
