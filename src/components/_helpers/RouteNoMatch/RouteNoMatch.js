import React from 'react';
import classNames from 'classnames';
import st from './routenomatch.css';

export default class RouteNoMatch extends React.Component {
    render() {
        const stAdvice = classNames(st.paragraph, st.advice);
        return (
            <div className={ st.block } id="RouteNoMatch">
                <p className={ st.paragraph }>404</p>
                <p className={ stAdvice }>Route not found.</p>
            </div>
        );
    }
}
