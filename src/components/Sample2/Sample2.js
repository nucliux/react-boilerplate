import React from 'react';
// import classNames from 'classnames';
import { Link } from 'react-router-dom';
import responsive from 'stylesResponsive.css';
import st from './sample2.css';

export default class Sample2 extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className={ responsive.row } id="Sample2">
                <h2 className={ st.title }>Sample2</h2>
                <Link to="/" id="home">Go Home</Link>
            </div>);
    }
}
