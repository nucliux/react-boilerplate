import 'store';
import React from 'react';
import { connect } from 'react-redux';
import Conf from 'conf';
import {
    BrowserRouter, // Hosted files on domain.
    HashRouter, // Static file server, ej: index.html.
    Route, Switch } from 'react-router-dom';
import { Home } from 'layouts';
import { RouteNoMatch } from 'helpers';

@connect((store) => ({}))
export default class App extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        if (Conf.staticFilesServer) {
            return (
                <HashRouter>
                    <Switch>
                        <Route path="/" component={ Home } />
                        <Route component={ RouteNoMatch } />
                    </Switch>
                </HashRouter>
            );
        }
        return (
            <BrowserRouter>
                <Switch>
                    <Route path="/" component={ Home } />
                    <Route component={ RouteNoMatch } />
                </Switch>
            </BrowserRouter>
        );
    }
}
