import store from 'store';
import React from 'react';
import { Provider } from 'react-redux';
import { mount } from 'enzyme';
import renderer from 'react-test-renderer';
import { BrowserRouter } from 'react-router-dom';

import Home from './Home';
import reducer from './homeReducer';
import * as actions from './homeActions';

describe('Home', () => {
    describe('visual', () => {
        // Variables
        let enzymeComponent = null;
        let tree = null;
        beforeAll(() => {
            const element = <Provider store={ store }><BrowserRouter><Home /></BrowserRouter></Provider>;
            enzymeComponent = mount(element);
            tree = renderer.create(element).toJSON();
            expect(tree).toMatchSnapshot();
        });
        test('Render text', () => {
            const str = 'Home';
            expect(enzymeComponent.find('#Home').text()).toContain(str);
        });
    });

    describe('reducers', () => {
        // Variables available for all tests.
        const reducerState = store.getState().Home;
        test('HOME_ADD_ERROR', () => {
            const payload = { value: 1, id_test: 'test' };
            const response = reducer(reducerState, { type: 'HOME_ADD_ERROR', payload });
            expect(response.error).toEqual(payload);
        });
        test('HOME_REMOVE_ERROR', () => {
            const response = reducer(reducerState, { type: 'HOME_REMOVE_ERROR' });
            expect(response.error).toEqual(false);
        });
        test('HOME_ADD_NOTIFICATION', () => {
            const payload = 'String advertisement...';
            const response = reducer(reducerState, { type: 'HOME_ADD_NOTIFICATION', payload });
            expect(response.notifications).toEqual([payload]);
        });
        test('HOME_REMOVE_NOTIFICATIONS', () => {
            const response = reducer(reducerState, { type: 'HOME_REMOVE_NOTIFICATIONS' });
            expect(response.notifications).toEqual([]);
        });
    });
    let props;
    let loadingProps;
    describe('actions', () => {
        beforeEach(() => {
            props = store.getState().Home;
            loadingProps = store.getState().Loading;
        });
        test('addError(text)', (done) => {
            const str = 'String ERROR!';
            const unsubscribe = store.subscribe(() => {
                props = store.getState().Home;
                try {
                    expect(props.error).toEqual(str);
                } catch (e) {
                    console.log(e);
                    throw Error(e);
                }
                unsubscribe();
                done();
            });
            store.dispatch(actions.addError(str));
        });
        test('addNotification(str)', (done) => {
            const str = 'string notification.';
            const unsubscribe = store.subscribe(() => {
                props = store.getState().Home;
                try {
                    expect(props.notifications.reverse()[0]).toEqual(str);
                } catch (e) {
                    console.log(e);
                    throw Error(e);
                }
                unsubscribe();
                done();
            });
            store.dispatch(actions.addNotification(str));
        });
        test('removeError()', (done) => {
            const unsubscribe = store.subscribe(() => {
                const props = store.getState().Home;
                try {
                    expect(props.error).toEqual(false);
                } catch (e) {
                    console.log(e);
                    throw Error(e);
                }
                unsubscribe();
                done();
            });
            store.dispatch(actions.removeError());
        });
        test('removeNotification()', (done) => {
            const unsubscribe = store.subscribe(() => {
                const props = store.getState().Home;
                try {
                    expect(props.notifications).toEqual([]);
                } catch (e) {
                    console.log(e);
                    throw Error(e);
                }
                unsubscribe();
                done();
            });
            store.dispatch(actions.removeNotification());
        });
        test('loading(true)', (done) => {
            const unsubscribe = store.subscribe(() => {
                loadingProps = store.getState().Loading;
                try {
                    expect(loadingProps.active).toEqual(true);
                } catch (e) {
                    console.log(e);
                    throw Error(e);
                }
                unsubscribe();
                done();
            });
            store.dispatch(actions.loading(true));
        });
        test('loading(false)', (done) => {
            const unsubscribe = store.subscribe(() => {
                loadingProps = store.getState().Loading;
                try {
                    expect(loadingProps.active).toEqual(false);
                } catch (e) {
                    console.log(e);
                    throw Error(e);
                }
                unsubscribe();
                done();
            });
            store.dispatch(actions.loading(false));
        });
    });
});
