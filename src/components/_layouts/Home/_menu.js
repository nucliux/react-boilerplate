import React from 'react';
import { NavLink } from 'react-router-dom';

import st from './home.css';

export default class Menu extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <header className={ st.header }>
                <nav>
                    <ul>
                        <li><NavLink exact to="/" id="home" activeClassName="selected">Home</NavLink></li>
                        <li><NavLink to="/sample" id="sample" activeClassName="selected">Sample</NavLink></li>
                        <li><NavLink to="/sample2" id="sample2" activeClassName="selected">Sample2</NavLink></li>
                    </ul>
                </nav>
            </header>
        );
    }
}
