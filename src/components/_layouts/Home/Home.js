import 'store';
import responsive from 'stylesResponsive.css';

import React from 'react';
import { connect } from 'react-redux';
import { Loading } from 'helpers';
import { Sample, Sample2 } from 'components';
import classNames from 'classnames';
import { Switch, Route } from 'react-router-dom';
import Menu from './_menu';

import * as actions from './homeActions';
import st from './home.css';

@connect((store) => ({
    error: store.Home.error,
    notifications: store.Home.notifications,
}))
export default class Home extends React.Component {
    constructor(props) {
        super(props);
        this.showNotifications = this.showNotifications.bind(this);
        this.showErrors = this.showErrors.bind(this);
    }

    componentWillMount() {
        // Activate helper <Loading />.
        this.props.dispatch(actions.loading(true));
        // Send notifications.
        this.props.dispatch(actions.addNotification('1 String sample text!'));
        this.props.dispatch(actions.addNotification('2 String sample text!'));
        // Send error.
        this.props.dispatch(actions.addError('Some advertisement...'));
    }

    showNotifications() {
        if (this.props.notifications) {
            const n = this.props.notifications;
            return n.map((row, k) => (<div key={ k } className={ st.notification }>{ row }</div>));
        }
        return null;
    }

    showErrors() {
        if (this.props.error) {
            return <div className={ st.error }>Error: { this.props.error }</div>;
        }
        return null;
    }

    render() {
        const laterals = classNames(responsive.col_2, responsive.col_m_1);
        const center = classNames(responsive.col_8, responsive.col_m_10);
        return (
            <div className={ responsive.row } id="Home">
                <div className={ laterals } />
                <div className={ center }>
                    {/* Header */}
                    <Menu />
                    {/* Component */}
                    <div className={ st.center } >
                        <fieldset>
                            <legend>Layout Home</legend>
                            {/* Self function interactions */}
                            { this.showNotifications() }
                            { this.showErrors() }
                            {/* Route component */}
                            <Switch>
                                <Route path="/sample2" component={ Sample2 } />
                                <Route path="/sample" component={ Sample } />
                            </Switch>
                            {/* Helper */}
                            <Loading />
                        </fieldset>
                    </div>
                </div>
                <div className={ laterals } />
            </div>
        );
    }
}
