import Conf from 'conf';
import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import reducer from 'reducers';

const logger = createLogger({
    collapsed: true,
});
let middleware;

if (Conf.debug) {
    middleware = composeWithDevTools(applyMiddleware(thunk, logger));
} else {
    middleware = applyMiddleware(thunk);
}

export default createStore(reducer, middleware);
